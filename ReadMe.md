# TFFR Pointe-à-Pitre Le Raizet Airport

This is Pointe-à-Pitre TFFR airport scenery for FlightGear Flight Simulator with

## Installation

- Download the repository .zip file with the link '...' -> 'Download repository'
- Extract the downloaded file to a directory of your choice
- Add this directory to 'Add-ons' -> 'Additional scenery folders' in FlightGear GUI launcher.

## Screenshots

![...](./media/TFFR-before.png)

![...](./media/TFFR-after_day.png)

![...](./media/TFFR-after_night.png)

## List of Airports I am Working On:

- MDPP - Gregorio Luperón International Airport, Puerto Plata Airport: https://bitbucket.org/fableb/flcs-mdpp_airport/src/master/
- MDSD - Las Américas International Airport: https://bitbucket.org/fableb/flcs-mdsd_airport/src/master/
- KMIA - Miami International Airport: https://bitbucket.org/fableb/flcs-kmia_airport/src/master/
- KWER - Newark Liberty International Airport: https://bitbucket.org/fableb/flcs-kewr_airport/src/master/
- TFFR - Pointe-à-Pitre International Airport: https://bitbucket.org/fableb/flcs-tffr_airport/src/master/

